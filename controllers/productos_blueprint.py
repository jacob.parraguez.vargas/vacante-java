from flask import Blueprint, jsonify
from models.productos import obtener_productos_db

productos_blueprint = Blueprint('productos', __name__)

@productos_blueprint.route('/productos', methods=['GET'])
def obtener_productos():
    productos = obtener_productos_db()
    return jsonify(productos)