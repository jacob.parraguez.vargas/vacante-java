from flask import Blueprint, request, jsonify
from models.user import get_client_status
from models.cuenta import add_saldo

clientes_blueprint = Blueprint('cliente', __name__)

@clientes_blueprint.route('/cliente', methods=['PUT'])
def cargar_saldos():
    data = request.json
    id_cliente = data.get('id_cliente', None)
    monto_a_sumar = data.get('monto_a_sumar', None)

    if id_cliente is None:
        return jsonify({"error": "user_id no proporcionado"}), 400

    if get_client_status(id_cliente):
        if add_saldo(id_cliente, monto_a_sumar):
            return jsonify({"mensaje" : "Saldo Cargado"})
        else:
            return jsonify({"mensaje" : "Error al cargar saldo"})
    else:
        return jsonify({"mesanje" : "Cliente Inactivo"})
