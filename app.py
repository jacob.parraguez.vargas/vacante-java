from flask import Flask
from controllers.productos_blueprint import productos_blueprint
from controllers.clientes_blueprint import clientes_blueprint

app = Flask(__name__)

app.register_blueprint(productos_blueprint)
app.register_blueprint(clientes_blueprint)

if __name__ == '__main__':
    app.run(debug=True)