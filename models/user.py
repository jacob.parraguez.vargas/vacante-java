from .db import connect

def get_client_status(id_cliente):
    connection = connect()
    cursor = connection.cursor()

    # Query
    cursor.execute("SELECT status_cliente FROM cliente WHERE id_cliente = %s", (id_cliente,))
    results = cursor.fetchone()

    cursor.close()
    connection.close()

    if results[0]=="Activo":
        return True
    else:
        return False

def get_clients():
    connection = connect()
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM cliente")
    results = cursor.fetchall()

    cursor.close()
    connection.close()

    cliente = []
    for row in results:
        client = {
            'id_cliente': row[0],
            'nombre_cliente': row[1],
            'status_cliente': row[2]
        }
        cliente.append(client)

    return cliente