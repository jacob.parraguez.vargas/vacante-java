from .db import connect

def obtener_productos_db():
    # Establecer conexión con la base de datos
    connection = connect()
    cursor = connection.cursor()

    # Query
    cursor.execute("SELECT * FROM Productos")
    results = cursor.fetchall()

    # Cerrar el cursor y la conexión
    cursor.close()
    connection.close()

    # Convertir los resultados a formato JSON
    productos = []
    for row in results:
        producto = {
            'id_producto': row[0],
            'nombre_producto': row[2],
            'precio_producto': float(row[1])
        }
        productos.append(producto)

    return productos