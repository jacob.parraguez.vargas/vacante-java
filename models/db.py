import mysql.connector

config = {
    'user': 'admin',
    'password': '1234',
    'host': 'localhost',
    'database': 'MiDB',
    'raise_on_warnings': True
}

def connect():
    return mysql.connector.connect(**config)