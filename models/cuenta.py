from .db import connect

def add_saldo(id_cliente, monto_a_sumar):
    connection = connect()
    cursor = connection.cursor()
    print(id_cliente, monto_a_sumar)
    # Query
    query = "UPDATE cuenta SET saldo_cliente = saldo_cliente + %s WHERE id_cliente = %s;"
    full_query = query % (monto_a_sumar, id_cliente)
    cursor.execute(full_query)
    results = cursor.fetchall()
    print(results)

    if cursor.rowcount == 1:
        cursor.close()
        connection.close()
        return True
    else:
        cursor.close()
        connection.close()
        return False

